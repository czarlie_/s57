let collection = []

function print() {
  return collection
}

function enqueue(element) {
  collection[collection.length] = element
  return collection
  // let newArr = []
  // for (let i = 0; i < collection.length; i++) {
  //   if (i == 0) continue
  //   collection += collection[i]
  // }
  // return collection

  // collection[collection.length] = element
  // for (let i = 0; i < collection.length; i++) return collection
}

function dequeue() {
  for (let i = 0; i < collection.length; i++) {
    let newArr = []
    if (i == 0) continue
    else newArr += collection[i]
    return [newArr]
  }
}

function front() {
  for (let i = 1; i < collection.length; i++) {
    return collection[i]
  }
}

function size() {
  return collection.length - 1
}

function isEmpty() {
  return collection.length === 0 ? true : false
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
}
